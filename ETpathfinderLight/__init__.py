import os
import numpy as np
from gwinc import nb, const
from gwinc.ifo.noises import *

from .SuspensionThermal.suspension_thermal import tf
from .Newtonian.nn import grav_rayleigh, grav_rayleigh_corr
from .kat_load import update_transmittance

def get_local_filename(*parts):
    return os.path.join(os.path.dirname(__file__), *parts)


class SeismicV(nb.Noise):
    style = dict(
        label='Seismic Vertical',
        color='#7FFFD4',
        linestyle='--',
        lw=2
    )
    def load(self):
        # seismic data file contains raw triaxial amplitude spectral densities
        sei_data = get_local_filename('Seismic', 'seismic_data.npy')
        _, _, asdV, freq = np.load(sei_data)
        # interpolate to our simulation frequencies
        asdV = self.interpolate(freq, asdV)
        # tf takes input freq, temperature, in-out indexes for vertical, in-out indexes
        # for horizontal; TM is the 4th mass in vertical TF and the 6th mass in horizontal
        vertical_TF = tf(self.freq, 123, 0, 4, 0, 6)[2]
        vertical_TF = np.abs(vertical_TF)**2
        # assume 0.1% V-to-H coupling and effect on 4 test masses
        self.psd = 4*(asdV**2) * 1e-6*vertical_TF

    def calc(self):
        return self.psd

class SeismicH(nb.Noise):
    style = dict(
        label='Seismic Horizontal',
        color='#8A2BE2',
        linestyle='--',
        lw=2
    )
    def load(self):
        sei_data = get_local_filename('Seismic', 'seismic_data.npy')
        asdE, asdN, _, freq = np.load(sei_data)
        asd_horizontal = self.interpolate(freq, np.sqrt(asdE**2 + asdN**2))
        horizontal_TF = tf(self.freq, 123, 0, 4, 0, 6)[0]
        horizontal_TF = np.abs(horizontal_TF)**2
        self.psd = 4* asd_horizontal**2 * horizontal_TF

    def calc(self):
        return self.psd

class Seismic(nb.Budget):
    style = dict(
        label = 'Seismic noise',
        color = '#A52A2A'
    )

    noises = [
        SeismicV,
        SeismicH
    ]

class Newtonian(nb.Noise):
    style = dict(
        label = 'Newtonian',
        color = 'xkcd:teal green',
        lw = 2.2
    )

    def calc(self):
        # Use grav_rayleigh() instead to not take correlations into account,
        # but in this case change the *2 in NN_psd to *4!
        NN_density = grav_rayleigh_corr(self.freq, self.ifo)
        sei_data = get_local_filename('Seismic', 'seismic_data.npy')
        _, _, asdV, freq = np.load(sei_data)
        asd_vertical = self.interpolate(freq, asdV)
        psd_vertical = asd_vertical**2
        NN_psd = NN_density*psd_vertical*2/(2*np.pi*self.freq)**4
        return NN_psd


class SuspensionThermalH(nb.Noise):
    style = dict(
        label='Suspension TN hor.',
        color='#a57720',
        linestyle='--',
        lw=2
    )

    def calc(self):
        noise = tf(self.freq, 123, 4, 4, 6, 6)[1]
        return 4*noise**2 #inserting PSD and apply on 4 TM


class SuspensionThermalV(nb.Noise):
    style = dict(
        label='Suspension TN vert.',
        color='#a57720',
        linestyle=':',
        lw=2
    )

    def calc(self):
        noise = tf(self.freq, 123, 4, 4, 6, 6)[3]
        return 4*(1e-3*noise)**2 # 0.1% of vertical coupled to longitudinal


class SuspensionThermal(nb.Budget):
    style = dict(
        label = 'Suspension Thermal Python',
        color = '#855700'
    )

    noises = [
        SuspensionThermalH,
        SuspensionThermalV
    ]

class ITMThermoRefractive(nb.Noise):
    # This is a direct copy of what's in pygwinc, but with parameter
    # `exact` set to true for the calculation.
    style = dict(
       label='ITM Thermo-Refractive',
       color='#ebe834',
       linestyle='--',
    )

    def calc(self):
        power = ifo_power(self.ifo)
        cavity = arm_cavity(self.ifo)
        n = noise.substratethermal.substrate_thermorefractive(
            self.freq, self.ifo.Materials, cavity.wBeam_ITM, exact=True)
        return n * 2 / (power.finesse * 2/np.pi)**2

class Substrate(nb.Budget):
    style = dict(
        label = 'Substrate Thermal',
        color = 'orange',
    )
    noises = [
        ITMThermoRefractive,
        SubstrateThermoElastic,
        SubstrateBrownian
    ]


class CLIO(nb.Noise):
    '''
    CLIO cryogenic noise from PhysRevLett.108.141101
    '''
    style = dict(
        label='CLIO cryogenic',
        color='gray',
        linestyle=':',
        lw=1
    )
    def load(self):
        clio_data = get_local_filename('CLIO','CLIOcoldASD.npy')

        freq, asd = np.load(clio_data)
        self.psd = self.interpolate(freq, (asd**2))

    def calc(self):
        return self.psd


class ETpathfinderLight(nb.Budget):
    name = 'ETpathfinder-Light'

    # list of noise sources
    noises = [
        QuantumVacuum,
        SuspensionThermal,
        CoatingBrownian,
        CoatingThermoOptic,
        ExcessGas,
        Seismic,
        Substrate,
        Newtonian,
    ]

    # recalibration of noise budget, e.g. to strain
    calibrations = [
        #Strain,
    ]

    # reference curves that do not get added into the total noise curve
    references = [
        #QuantumFinesse,
        #*SuspensionThermal.noises,
        #*Seismic.noises
        #CLIO
        *Substrate.noises
    ]

    plot_style = dict(
        ylabel=u"Displacement Sensitivity [m/\u221AHz]",
    )

    def load(self):
        for mirror in [self.ifo.Optics.ETM, self.ifo.Optics.ITM]:
            update_transmittance(self.ifo, mirror)
        print(f'ITM Transmittance: {self.ifo.Optics.ITM.Transmittance*1e6:.0f}ppm')
        print(f'ETM Transmittance: {self.ifo.Optics.ETM.Transmittance*1e6:.0f}ppm')
        super().load()
