import numpy as np
import scipy
from gwinc import const

def grav_rayleigh(freq, ifo):
    G = const.G
    gamma = ifo.Seismic.Gamma
    rho  = ifo.Seismic.Rho
    h = ifo.Seismic.TestMassHeight
    c_rayleigh = ifo.Seismic.RayleighWaveSpeed
    k = (2*np.pi*freq)/c_rayleigh
    S_n = ((2*np.pi*G*rho*np.exp(-h*k)*gamma)**2)*0.5 # -- eq 112 in J Harms https://doi.org/10.1007/s41114-019-0022-2
    return S_n

def grav_rayleigh_corr(freq, ifo):
    L = ifo.Infrastructure.Length
    # distance between the two side-by-side ITMs/ETMs of one arm
    L_short = 2*ifo.Infrastructure.Vacuum.BeamTubes_dx
    c_rayleigh = ifo.Seismic.RayleighWaveSpeed
    k = (2*np.pi*freq)/c_rayleigh
    S_n = grav_rayleigh(freq, ifo)
    factor = (1-2*scipy.special.j0(k*L) + 2*scipy.special.j1(k*L)/(k*L)) # -- eq 101 in J Harms https://doi.org/10.1007/s41114-019-0022-2
    factor2 = (1-2*scipy.special.j1(k*L_short)/(k*L_short))
    factorT = factor*factor2
    S_n_corr = S_n*factorT*2 #added 2 to remove the 1/2 component from Eq 112
    return S_n_corr
