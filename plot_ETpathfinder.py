# Plot the current ETpathfinderA noise budget
import sys
import numpy as np
import matplotlib.pyplot as plt
import gwinc

budgets = {
    'A': 'ETpathfinderA',
    'B': 'ETpathfinderB',
    'L': 'ETpathfinderLight'
}

ifos = list(budgets.keys())
valid_options = str.join(', ', budgets.keys())

if len(sys.argv) > 1:
    ifos = []
    for ifo in sys.argv[1:]:
        if ifo in budgets:
            ifos.append(ifo)
        else:
            print(f'ERROR: unknown interferometer {ifo}, valid options are {valid_options}')
else:
    print(f'=== NO IFOs GIVEN, USING DEFAULT OF {valid_options} ===\n')

freq = np.logspace(0,4,3000)
fig, axes = plt.subplots(1, len(ifos), figsize=(6*len(ifos), 5), sharey=True)
if not isinstance(axes, np.ndarray):
    axes = [axes]

style = {
    'ylabel': u"Displacement Sensitivity [m/\u221AHz]",
    'ylim': [1e-20, 1e-15],
}

for idx, ifo in enumerate(ifos):
    budget = gwinc.load_budget(budgets[ifo], freq=freq)
    traces = budget.run()
    gwinc.plot_budget(traces, ax=axes[idx], **style, title=budget.name)
    axes[idx].get_legend().remove()
    if 'ylabel' in style:
        del(style['ylabel']) # no ylabel on further plots

handles, labels = axes[-1].get_legend_handles_labels()
#if len(axes) == 1:
axes[-1].legend(handles, labels, fontsize='small', ncol=2)
#else:
#    plt.subplots_adjust(bottom=0.2)
#    fig.legend(handles, labels, fontsize='small',
#               loc='lower center', ncol=len(ifos)*2)
plt.tight_layout()
plt.savefig('ETPathfinder.png', dpi=300)
plt.show()

