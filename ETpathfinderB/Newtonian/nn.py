from gwinc import nb, const
import numpy as np
import scipy

def grav_rayleigh(freq, ifo):
    G = const.G
    gamma = ifo.Seismic.Gamma
    rho  = ifo.Seismic.Rho
    h = ifo.Seismic.TestMassHeight
    c_rayleigh = ifo.Seismic.RayleighWaveSpeed
    k = (2*np.pi*freq)/c_rayleigh
    S_n = ((2*np.pi*G*rho*np.exp(-h*k)*gamma)**2)*0.5 # -- eq 112 in J Harms https://doi.org/10.1007/s41114-019-0022-2
    return S_n

def grav_rayleigh_corr(freq, ifo):
    L1 = 9.22
    L2 = 0.45
    c_rayleigh = ifo.Seismic.RayleighWaveSpeed
    k = (2*np.pi*freq)/c_rayleigh
    S_n = grav_rayleigh(freq, ifo)
    factor1 = (1-2*scipy.special.j0(k*L1) + 2*scipy.special.j1(k*L1)/(k*L1))
    factor2 = (1-2*scipy.special.j1(k*L2)/(k*L2))# -- eq 101 in J Harms https://doi.org/10.1007/s41114-019-0022-2
    factor_T = factor1*factor2
    S_n_corr = S_n*factor_T*2 #corrected this
    return S_n_corr
