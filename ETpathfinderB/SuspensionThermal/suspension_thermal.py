from gwinc import nb, const
import numpy as np
import os
from wield.iirrational.TFmath.statespace import ss2xfer

# Define first the relevant parameters

def tf(freq, T, in_v, out_v, in_h, out_h):

    def convert_to_displacement_noise(f2x, freq, T):
        s = 2j*np.pi*freq
        Zm = 1/(f2x * s)
        Fm = np.sqrt(4*const.kB * T * np.real(Zm))
        return abs(f2x * Fm)

    # long IP platform, small IP platform, F2, F3, CF, MAR, MIR, MRM + RM as one body
    Masses = [600., 30., 20., 20., 20., 10., 3.293, 9.5] # [kg]
    # long IP, small IP, F2, F3, CF, MAR, MIR, MRM, RM
    Lengths = [4, 0.5, 1.2, 0.85, 0.9, 0.6, 0.4, 0.62] # [m]
    # long IP 4x leg mass, small IP 3x leg mass
    IPMasses = [48., 0.8] # [kg]
    IPFrequencies =  [0.07, 0.1]
    Omega = np.array(IPFrequencies)*2*np.pi

    #YoungsModuli = [0, 0, 0, 0, 0, 110e9, 135e9, 110e9]
    YoungsModuli = [0, 0, 0, 0, 0, 110e9, 155.8e9, 110e9]
    #LossAngles = [1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-5, 1e-5, 1e-4]
    LossAngles = [1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-5, 1e-9, 1e-4]
    WireCount = [0, 0, 0, 0, 0, 1, 4, 3]
    #WireDiameter = [0, 0, 0, 0, 0, 800e-6, 75e-6, 600e-6]
    WireDiameter = [0, 0, 0, 0, 0, 0.8e-3, 0.7e-3, 600e-6]



    w = 2*np.pi*freq
    s = 1j*w
    #seis = 1e-8/(w**2) - use this instead of real data

    # function to return the state-space matrices
    def get_statespace_matrices_H(masses, lengths, phi, mu, omega, N, E, d):
        num_DOF = len(masses)
        num_states = 2*num_DOF
        num_inputs = num_DOF

        K, kh = get_stiffness_matrix_H(num_DOF, phi, masses, mu, lengths, omega, N, E, d)
        M = get_masses_matrix_H(masses, mu)
        iM = np.linalg.pinv(M)

        # viscous damping matrix
        damp = 1e-10 * np.eye(num_DOF)
        # external force matrix
        F = np.eye(num_DOF)
        A = np.block([
            [np.zeros((num_DOF, num_DOF)), np.eye(num_DOF)],
            [-iM @ K, -iM @ damp]
        ])
        B = np.block([
            [np.zeros((num_DOF, num_inputs))],
            [iM @ F]
        ])
        C = np.eye(num_DOF, num_states)
        D = np.zeros((num_DOF, num_inputs))
        return A,B,C,D,kh

    def get_statespace_matrices_V(masses, lengths, phi, N, E, d):
        num_DOF = len(masses[2:])
        num_states = 2*num_DOF
        num_inputs = num_DOF

        K, kv = get_stiffness_matrix_V( phi, masses, lengths, N, E, d)
        M = get_masses_matrix_V(masses)
        iM = np.linalg.pinv(M)

        # viscous damping matrix
        damp = 1e-10 * np.eye(num_DOF)
        # external force matrix
        F = np.eye(num_DOF)
        A = np.block([
            [np.zeros((num_DOF, num_DOF)), np.eye(num_DOF)],
            [-iM @ K, -iM @ damp]
        ])
        B = np.block([
            [np.zeros((num_DOF, num_inputs))],
            [iM @ F]
        ])
        C = np.eye(num_DOF, num_states)
        D = np.zeros((num_DOF, num_inputs))
        return A,B,C,D, kv



    def get_masses_matrix_H(masses, mu):
        # Masses in matrix form
        M = np.diag(masses)
        M[0, 0] = masses[0] + (mu[0]+mu[1])/3.
        M[0, 1] = mu[1]/6.
        M[1, 0] = mu[1]/6.
        M[1, 1] = masses[1] + mu[1]/3.
        return M

    def get_stiffness_matrix_H(num_DOF, phi, masses, mu, lengths, omega, N, E, d):
        k = get_stiffness(num_DOF, phi, masses, mu, lengths, omega, N, E, d)
        return np.array([
            [k[0]+k[1], -k[1], 0, 0, 0, 0, 0, 0],
            [-k[1], k[1]+k[2], -k[2], 0, 0, 0, 0, 0],
            [0, -k[2], k[2]+k[3], -k[3], 0, 0, 0, 0],
            [0, 0, -k[3], k[3]+k[4], -k[4], 0, 0, 0],
            [0, 0, 0, -k[4], k[4]+k[5]+k[7], -k[5], 0, -k[7]],
            [0, 0, 0, 0, -k[5], k[5]+k[6], -k[6], 0],
            [0, 0, 0, 0, 0, -k[6], k[6], 0],
            [0, 0, 0, 0, -k[7], 0, 0, k[7]]
        ]), k[0]


    def get_stiffness(num_DOF, phi, masses, mu, lengths, omega, N, E, d):
        def stiffness(masses, length, omega=0.0):
            return np.sum(masses) * (const.g/length + omega**2)

        def bending_stiffness(n, phi, E, m, d):
            return 2*(1+1j*phi)*np.sqrt(n*np.pi*E*m*const.g*(d**4)/64)

        k = np.zeros(num_DOF, dtype=complex)

        # IP overall flexure stiffness [N/m]
        for ii in range(0,2):
            k[ii] = (1+1j*phi[ii])*stiffness(np.r_[masses[ii:], mu[ii:]/2], lengths[ii], omega[ii])
            k[ii] -= stiffness(np.r_[masses[ii:], mu[ii:]/2], lengths[ii])
        for ii in range(2, 5):
            k[ii] = (1+1j*phi[ii])*stiffness(masses[ii:], lengths[ii])

    # bending stiffness
        _m = [0, 0, 0, 0, 0, masses[5]+masses[6], masses[6], masses[7]]
        for ii in range(5, 8):
            ka = bending_stiffness(N[ii], phi[ii], E[ii], _m[ii], d[ii])
            k[ii] = _m[ii] * const.g/lengths[ii] + ka/(lengths[ii]**2)

        return k

#vertical definitions
    mu_1 = np.sum(Masses[2:])/40
    mu_2 = np.sum(Masses[3:])/40
    mu_3 = np.sum(Masses[4:])/40
    mu_4 = (Masses[5]+Masses[6])/40

    def get_masses_matrix_V(masses):
    # Masses in matrix form
        M = np.diag(masses[2:])
        M[0, 0] = masses[2] + (mu_1+mu_2)/3
        M[0, 1] = 0.
        M[1, 0] = 0.
        M[1, 1] = masses[3] + mu_1/3 + mu_2/3
        M[1,2] = 0
        M[2,1] = 0
        M[2,2] = masses[4] + (mu_3 + mu_4)/3
        M[2,3] = 0
        M[3,2] = 0
        M[3,3] = masses[5] + mu_4/3
        return M

    def get_stiffness_matrix_V(phi, masses, lengths, n,E, d):

        w0 = 2*np.pi*0.25
        k2 = (1+1j*0.03)*np.sum(Masses[2:])*(w0**2)
        k3 = (1+1j*0.03)*np.sum(Masses[3:])*(w0**2)
        k4 = (1+1j*0.03)*np.sum(Masses[4:])*(w0**2)
        k5 = (1+1j*0.03)*(masses[5]+masses[6])*(w0**2)

        k6=n[6]*E[6]*(1+1j*phi[6])*(0.25*np.pi*d[6]**2)/lengths[6]
        k7=n[7]*E[7]*(1+1j*phi[7])*(0.25*np.pi*d[7]**2)/lengths[7]*0

        return np.array([
        [k2+k3, -k3, 0, 0, 0, 0],
        [-k3, k3+k4, -k4, 0, 0, 0],
        [0, -k4, k4+k5+k7, -k5, 0, -k7],
        [0, 0, -k5, k5+k6, -k6, 0],
        [0, 0, 0, -k6, k6, 0],
        [0, 0, -k7, 0, 0, k7],
        ]), k2
        print('k6_vert', k6)


    M = np.array(Masses)
    L = np.array(Lengths)
    phi = np.array(LossAngles)
    mu = np.array(IPMasses)
    O = Omega
    N = np.array(WireCount)
    Y = np.array(YoungsModuli)
    d = np.array(WireDiameter)

    Ah,Bh,Ch,Dh, kh = get_statespace_matrices_H(M, L, phi,mu,O, N, Y, d)
    Av,Bv,Cv,Dv, kv = get_statespace_matrices_V(M, L, phi, N, Y, d)

    force_transfer_long_H = ss2xfer(Ah, Bh, Ch, Dh, F_Hz=freq, idx_in=in_h, idx_out=out_h)
    force_transfer_long_V = ss2xfer(Av, Bv, Cv, Dv, F_Hz=freq, idx_in=in_v, idx_out=out_v)

    x_transfer_V = kv*force_transfer_long_V
    x_transfer_H = kh*force_transfer_long_H

    displacementH = convert_to_displacement_noise(force_transfer_long_H, freq, T)
    displacementV = convert_to_displacement_noise(force_transfer_long_V, freq, T)
    dispalcement_pro = np.sqrt(displacementH**2 + (0.001*displacementV)**2) #total horizontal, assuming 0.1% vertical coupling
    return x_transfer_H, displacementH, x_transfer_V, displacementV, 4*dispalcement_pro