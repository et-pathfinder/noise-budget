import math
import os

from gwinc import Struct
from gwinc.noise.coatingthermal import getCoatRefl

def update_transmittance(ifo, mirror):
    if 'CoatLayerOpticalThickness' in mirror:
       ampl_refl = getCoatRefl(ifo.Materials,
                               mirror.CoatLayerOpticalThickness)[0]
       mirror.Transmittance = 1.0-abs(ampl_refl)**2


def get_constants(ifo):
    update_transmittance(ifo,ifo.Optics.ETM)
    update_transmittance(ifo,ifo.Optics.ITM)
    
    ifo_constants = {
        'P_in': ifo.Laser.Power, # laser power into BS
        'LimcAB': ifo.Optics.IMC.sLong, 
        'LimcBC': math.sqrt((float(ifo.Optics.IMC.sLong)**2) + 
                            (float(ifo.Optics.IMC.sWidth)**2)),
        'IMC_AoI': ifo.Optics.IMC.AoI,
        'IMC_RoC': ifo.Optics.IMC.RoC,
        'MMA1_Rc': ifo.Optics.Aux.MMA1.RoC, # Mode matching telescope first mirror curvature
        'MMA2_Rc': ifo.Optics.Aux.MMA2.RoC, # Mode matching telescope second mirror curvature
        'Ly': ifo.Infrastructure.Length, # north arm length
        'Lx': ifo.Infrastructure.Length, # east arm length
        'L_smallMI': ifo.Infrastructure.Vacuum.CIOrigin, # length of small MI (BS -> IMs)
        'MICH_offset': ifo.Optics.MICHoffset, # diff. arm phase (sets darkport output power)
        'T_EM': ifo.Optics.ETM.Transmittance, # EM transmission
        'L_EM': ifo.Optics.Loss,  # EM loss
        'M_EM': ifo.Materials.MirrorMass, # EM mass
        'T_IM': ifo.Optics.ITM.Transmittance,  # IM transmission
        'L_IM': ifo.Optics.Loss, # IM loss
        'M_IM': ifo.Materials.MirrorMass, # IM mass   
    }
    constants = ["const {} {}".format(k,v) for k,v in ifo_constants.items()]
    return "\n".join(constants)
    
def get_basekatfile(noise_budget_path=None, kat_path = None):
    """ Return the basic finesse/pykat object with no detectors 
    
    Parameters
    ------------
        noise_budget_path (string), default: 'ifo.yaml'
        kat_path (string), default: 'ifo.kat'
    """
    
    # Load kat file
    if kat_path is None:
        kat_path = os.path.join(os.path.dirname(__file__), 'ifo.kat')
    with open(kat_path,'r') as f:
        katscript = f.read()
    
    #Load noise budget
    if noise_budget_path is None:
        noise_budget_path = os.path.join(os.path.dirname(__file__), 'ifo.yaml')
    ifo = Struct.from_file(os.path.realpath(noise_budget_path))
    
    # Return
    return get_constants(ifo) + "\n" + katscript
