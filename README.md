# ET Pathfinder Noise Budget

## Quick Glance at Some Parameters

These are just here as a quick reference. Definitive values are always in
the `ifo.yaml` file.

| Parameter         | ETpathfinderA  | ETpathfinderB  |
|-------------------|---------------:|---------------:|
| Wavelength        | 1550nm         | around 2µm     |
| TM Temperature    | ~18K           | ~120K          |
| TM Material       | silicon        | silicon        |
| TM Diameter       | 15cm           | 15cm           |
| Arm Cavity Length | 9.22m          | 9.22m          |
| ITM/ETM HR RoC    | 14.5m          | ?              |
| ITM AR RoC        | 9m             | ?              |
| Arm Cavity waist  | 1.825mm        | ?              |
| Beam Radius @ ITM | 2.21mm         | ?              |


## Parameters YAML File

To ensure that every simulation uses the same general parameters, all simulation parameters should be located in the YAML file `ETpathfinder[A,B]/ifo.yaml`. It follows the convention for parameter names as introduced by GWINC/pygwinc and should be kept compatible. Each entry should be associated with a comment describing the parameter.

Example usage in python:
```python
import gwinc
ifo = gwinc.load_budget('path/to/ETpathfinderA').ifo
print(ifo.Laser.Power)
```

## Noise Budget

For convenience, the file `plot_ETpathfinder[A,B].py` should always create a plot with the up-to-date noise budget. In the folder `ETpathfinder[A,B]`, all noise sources for the ETpathfinder should be defined as a `pygwinc` compatible file, i.e., to add a noise source, you should add it to `ETpathfinder[A,B]/__init__.py`, following the examples in there.

## Finesse Base File

The folder `ETpathfinder[A,B]` also contains a `.kat` file containing the latest Finesse simulation layout.

**ETPathfinderA**: The finesse file for ETPathfinderA has been updated, so that you can import the finesse base file and use it in other simulations. The boilerplate code is

```python
import sys, os
path_to_noise_budget = '../../noise-budget/' # relative to this file
path_to_noise_budget = os.path.realpath(path_to_noise_budget) #cannonicalize path
sys.path.insert(0,path_to_noise_budget) # add to path

#
# Constants
#
from ETpathfinderA.kat_load import get_basekatfile
```

The kat_load module does not depend on any specific version of Finesse, although it will return a legacy (Finesse2) file. This can be parsed with either Finesse2 or Finesse3. This finesse file will automatically load the constants from the ifo.yaml.

**ETPathfinderB**: This is a slightly older version of the same idea. There is a `.kat` which is meant to be used together with the `finesse.py` script in the same folder, which will load the `.kat` file into a `pykat.Finesse` object and make sure that parameters from the YAML file are loaded.

## Unit Testing

The tests folder contains a number of unit tests. When adding new code, please make sure it is covered by a unit test. This folder is a great place to start when looking for working examples.

You can run the unit tests by running `pytest -v test/` from this folder.

## Contributing to Noise Budget

To make sure we always have a known noise budget state in the gitlab, pushing of new code directly into `master` is not allowed. Instead, follow this procedure:

1. create a new local branch for your changes (`git checkout -b new-cool-feature`)
2. push this branch to gitlab. It will give you a URL with which you can create a merge request for this branch.
    - make your changes on this branch, pushing as often as you like
    - while it's still work in progress, prefix the merge request with `WIP:`
3. document your changes in this merge request **and** make a labbook entry
4. once everything looks good, it's time to remove the `WIP:` tag, and then it can be merged into the `master` branch.

## Installation/Usage

### Get Python 3
Most of the simulations in this repository require a current version of Python. Currently, the recommended Python distribution is Anaconda, which is available for Windows, Mac and Linux. Make sure to get a 64-bit, Python 3.x version. During the installation, it is recommended to select "install for current user only" (on Windows).

### Install Git

`git` is the file-versioning tool that we use to track changes to the simulations and to allow multiple people to work on it. It is used to _push/pull_ changes to this `gitlab` repository.

Start an `Anaconda Prompt` and type:

```
conda install git
conda install -c conda-forge git-lfs
```

### Install PyKat and Finesse
In an `Anaconda Prompt` type
```
conda install -c gwoptics pykat
```
to get both Finesse and pykat (and it will install a few other useful things as well). Quickly test the installation by typing `ipython` <kbd>Enter</kbd>, then once you see the `In [1]` message, type `import pykat` <kbd>Enter</kbd> and you should be presented with a cute cat and a pie. Type `exit` <kbd>Enter</kbd> to quit `ipython`.

### Install pygwinc

**Caution**: our ETpathfinder model needs `ss2xfer` from Lee McCuller, which previously was in his IIRrational package. It has since been moved into `wield-iirrational`, which can be installed via `pip`. Also install the dependency `wield-declarative`, which doesn't seem to get picked up automatically. Unfortunately, [this version issue](https://github.com/wieldphysics/wield-iirrational/commit/6ded5f4f9d847b4d2a0dcdd5dd504e21c149175b) has to be manually applied to the code afterwards, it's not yet in the packaged version.

`pygwinc` is based on the Matlab-Script `GWINC`, the _Gravitational Wave Interferometer Noise Calculator_, and aims at providing a much nicer interface and better code style. `pygwinc` is still in heavy development, so this installation instruction might not work for future installations. Please update if you have newer information :)

Again, in an Anaconda Prompt, type
```
pip install git+https://git.ligo.org/gwinc/pygwinc.git
```
Afterwards, try
```
gwinc aLIGO
```
... which should open up a sensitivity plot for Advanced LIGO. Once you have the ETpathfinder simulation repository, then
```
gwinc ETpathfinderA
```
should also give you a noise budget (but with unoptimized axes limits).

### Get repositories

Now it's time to create a copy of these repositories (= collection of simulation files) on your computer.

If you prefer to work with your own editor and are happy to use the command line, feel free to do so, but you're on your own now :)
Otherwise, read on...

First of all, if you haven't used https://git.ligo.org before, click on your user icon in the top right corner, choose `User Settings`, then `Access Tokens`. Now, add a new _personal access token_ with _api_ rights. It will show a cryptic password that you should keep safe somewhere, there's no way to retrieve it again otherwise.

Open `Anaconda Navigator` and click on the `VS Code` icon. This will start _Visual Studio Code_, a pretty nice editor that also supports working with git repositories. In the left icon bar, select `Source Control`, then `Clone Repository`. As URL, enter `https://git.ligo.org/et-pathfinder/the-repo.git`, replacing `the-repo` with e.g. `finesse-playground` and press <kbd>Enter</kbd>. You are now asked to select a folder where you want the simulation files to be on your computer. Then, type in your `albert.einstein` LIGO username and use the _personal access token_ (which you created above) as a password. It will then take a few moments to "clone" the repository (which means it creates a copy on your computer) and then offer to open the repository for you. Click yes, and you're good to go. Repeat for all other repositories that you need (likely at least `noise-budget`). Make sure they are all within the same parent directory, so that relative paths work for everyone in the same way.

### Quick Git Introduction

Please have a look at Duncan MacLeod's excellent git tutorial: https://duncanmmacleod.docs.ligo.org/gitlab-tutorial/

The following is a _very_ short introduction into using `git` with `VS Code`.Whenever you changed something, you can see those changes in the `Source Control` panel in `VS Code`. Clicking on a file will show you your changes side-by-side with the original file. At this time, these changes are not recorded anywhere and you can simply click `Discard changes` and it's back the way it was (of course, make sure that you're not loosing anything important!). To permanently add your changes to the repository, you first have to _stage_ them by clicking on the `+` icon. You can stage all your changes or just a selection of them. Once you are happy, you can _commit_ the changes by typing in a short descriptive message in the text field above and hit <kbd>Ctrl</kbd>+<kbd>Enter</kbd>. Now, your changes are permanently attached to the repository together with the message. However, so far they still only exist on your computer. To upload (_push_) your changes to gitlab, so that others can see them, click the three dots icon and select `Push`. To receive changes that others made, click `Pull`. Do this often, and especially do this before pushing new changes, to make sure everything is up-to-date.

