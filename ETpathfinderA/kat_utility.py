#
# A collection of utility functions
# for working with ET pathfinder models
# in Finesse2 + PyKat
#

from .kat_load import get_basekatfile
import tabulate, numpy as np
from pykat.finesse import kat
from scipy.optimize import minimize_scalar

def optimize_MICH_offset(basekat, darkport_dc_target=None):
    """ Optimizes the MICH offset to obtain a given DC carrier
    field in the dark port """

    if darkport_dc_target is None:
        darkport_dc_target = ifo.Optics.PhotoDetectorPower

    mykat = deepcopy(basekat)
    mykat.parse("""
    pd pdDC_dark nout
    """)
    mykat.noxaxis = True
    
    def _opt_pdDC(x):
        mykat.mBS.phi = 0.0+x
        out = mykat.run()
        logging.info('Detuning: {0:.5f} \t Power: {1:.4f}mW'.format(x, out['pdDC_dark']*1e3))
        return np.abs(out['pdDC_dark'] - darkport_dc_target)

    sol = minimize_scalar(_opt_pdDC, method = 'Bounded', bounds=(0,10), 
                          options={'maxiter': 100, 'xatol': 1e-05})

    logging.info(f'Offset found to be {sol.x:.3f}deg for {darkport_dc_target*1e3:.1f}mW output power')

    # update operating point
    basekat.constants['MICH_offset'].value = sol.x
    return sol.x


def get_DC_operating_point(kat,optimize_offset=False):
    """ Prints and returns the DC operating point, i.e. power levels at various points
    inside the interferometer for nominal lock.
    
    
    Returns an out object.
    """
    
    kat.parse("""
    # dark port
    pd Dark_Port nout
    # arm transmission
    pd Transm_E nEMAx1
    pd Transm_N nEMAy1
    # arm intracav
    pd IntraCav_E nEMAx2
    pd IntraCav_N nEMAy2
    # bright port reflected
    pd Refl nBSin

    noxaxis
    """)
    out = kat.run()
    print(out.ylabels,np.nditer(out.y))

    print(tabulate.tabulate(zip(out.ylabels, np.nditer(out.y)),
                            headers=['Location', 'Power (W)']))
    return out


def get_qnls(kat, freq=None, optimize_offset=False):
    """ Returns the Quantum-Noise Limited Sensitivity in m/sqrt(Hz) """

    if freq is None:
        min_f = 1
        max_f = 200e3
        steps = 1000
    else:
        min_f = freq[0]
        max_f = freq[-1]
        steps = len(freq)-1

    if optimize_offset:
        optimize_MICH_offset(kat)
    kat.parse(f"""
    fsig DARMn EMAy 1 0 0.5
    fsig DARMe EMAx 1 180 0.5

    qnoisedS QNLS 1 $fs max nout
    scale meter QNLS
    
    xaxis DARMn f log {min_f} {max_f} {steps}
    yaxis log abs
    """)
    
    out = kat.run()
    return out.x, out['QNLS']

def plot_qnls_A(file_out='qnlsA.png'):
    import matplotlib.pyplot as plt

    k1 = kat()
    k1.parse(get_basekatfile())

    freq, y = get_qnls(k1)

    get_DC_operating_point(k1)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.loglog(freq, y)
    ax.grid(which='both')
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Displacement sensitivity (m/$\sqrt{\\rm Hz}$)')
    plt.savefig(file_out)

if __name__=="__main__":
    plot_qnls_A()
