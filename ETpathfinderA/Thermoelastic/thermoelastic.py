import os
import numpy as np
import scipy
from gwinc import const
from gwinc.noise.substratethermal import substrate_thermoelastic_FiniteCorr

zeta = scipy.special.jn_zeros(1, 300) #BESSEL_ZERO
j0m = scipy.special.jn(0, zeta)

def J(Ome):
    A=np.sqrt(1/Ome**4*np.sqrt(2))
    B=np.sqrt(1/Ome/10)
    Result=(1/(1/np.sqrt(A)+1/np.sqrt(B)))**2
    return Result


def substratethermoelastic(f, materials, wBeam):

    sigma = materials.Substrate.MirrorSigma
    rho = materials.Substrate.MassDensity
    kappa = materials.Substrate.MassKappa # thermal conductivity
    alpha = materials.Substrate.MassAlpha # thermal expansion
    CM = materials.Substrate.MassCM # heat capacity @ constant mass
    Temp = materials.Substrate.Temp # temperature
    kBT = const.kB * materials.Substrate.Temp

    wc=2*kappa/(rho*CM*wBeam**2)

    Ome=2*np.pi*f/wc

    S = 8*(1+sigma)**2*kappa*alpha**2*Temp*kBT*Ome**2*J(Ome) # note kBT has factor Temp
    S /= (np.sqrt(2*np.pi)*(CM*rho)**2)
    S /= (wBeam/np.sqrt(2))**3 # LT 18 less factor 1/omega^2

    # Corrections for finite test masses:
    S *= substrate_thermoelastic_FiniteCorr(materials, wBeam)

    return S/(2*np.pi*f)**2
