import pytest
import os

thisdir = os.path.realpath(__file__)
path_to_NB = os.path.realpath(
        os.path.join(thisdir, '../../ETpathfinderA'))

print('path to noise budget', path_to_NB)

def test_run_budget():
        
    import gwinc
    budget = gwinc.load_budget(path_to_NB)
    trace = budget.run()
    
def test_get_katfileA():
    from ETpathfinderA.kat_load import get_basekatfile
    get_basekatfile()

def test_parse_basekatA():
    from ETpathfinderA.kat_load import get_basekatfile
    from pykat.finesse import kat 
    k1 = kat()
    k1.parse(get_basekatfile())

def test_get_qnls():
    from pykat.finesse import kat
    from ETpathfinderA.kat_utility import get_qnls, get_basekatfile
    k1 = kat()
    k1.parse(get_basekatfile())

    freq, y = get_qnls(k1)


def test_get_DC_operating_point():
    from pykat.finesse import kat
    from ETpathfinderA.kat_utility import get_DC_operating_point, get_basekatfile
    k1 = kat()
    k1.parse(get_basekatfile())

    out = get_DC_operating_point(k1)
    
def test_finesse_gwinc():
    """ Check that Finesse and GWINC produce the same quantum noise
    
    See et-pathfinder/finesse-playground/aaron_jones/3_compare_finesse_gwinc.ipynb
    https://git.ligo.org/et-pathfinder/finesse-playground/-/blob/bdc8527846fdb3192cea5b40e43e9ded146855cb/aaron_jones/3_compare_finesse_gwinc.ipynb
    """
    
    # get GWINC trace
    import gwinc
    budget = gwinc.load_budget(path_to_NB)
    trace = budget.run()
    
    # Get finesse trace
    from pykat.finesse import kat
    from ETpathfinderA.kat_utility import get_qnls, get_basekatfile
    k1 = kat()
    k1.parse(get_basekatfile())
    freq, y = get_qnls(k1)
    
    from scipy.interpolate import interp1d
    import numpy as np
    func = interp1d(freq,y)
    assert np.allclose(func(trace['QuantumVacuum'].freq),trace['QuantumVacuum'].asd,atol=0,rtol=0.2)
    # 0.2 was set by Aaron as a reasonable permissible error.
    # These 'look similar' on the plot, but otherwise there has been
    # no investigation into these differences. 
